<?php

namespace App\Repository;

use App\Entity\NumericalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NumericalCode>
 *
 * @method NumericalCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method NumericalCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method NumericalCode[]    findAll()
 * @method NumericalCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NumericalCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NumericalCode::class);
    }

    public function save(NumericalCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(NumericalCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllOrderedByNewest()
    {
        return $this->createQueryBuilder('nc')
            ->orderBy('nc.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return NumericalCode[] Returns an array of NumericalCode objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('n.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?NumericalCode
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
