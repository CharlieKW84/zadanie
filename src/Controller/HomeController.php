<?php

namespace App\Controller;

use App\Entity\NumericalCode;
use App\Repository\NumericalCodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="add_number")
     */
    public function addNumber(EntityManagerInterface $entityManager,Request $request,ValidatorInterface $validator)
    {
        // funkcja zapisuje przesłany kod, również gdy jest on niepoprawny; ustawia wtedy odpowiednią flagę
        if($request->getMethod()=='POST')
        {
            $numericalCode = new NumericalCode();
            $numericalCode->setCode($request->request->get('code'));
            if (count($validator->validate($numericalCode)) > 0) {
                $numericalCode->setIsCorrect(false);
            }
            else{
                $numericalCode->setIsCorrect(true);
            }
            $numericalCode->setCreatedAt(new \DateTimeImmutable());
            $entityManager->persist($numericalCode);
            $entityManager->flush();
        }

        $repository = $entityManager->getRepository(NumericalCode::class);
        $codes = $repository->findAllOrderedByNewest();

        return $this->render('number/add.html.twig', ['codes' => $codes]);
    }

    /**
     * @Route("/check/{code}", name="check_if_exists", methods="GET")
     */
    public function checkIfExists($code, NumericalCodeRepository $repository): Response
    {
        // funkcja sprawdza czy dany kod jest już w bazie
        if($repository->findOneBy(["code" => $code])){
            return $this->json(true);
        }
        else{
            return $this->json(false);
        }
    }

    /**
     * @Route("/code/{code}", name="delete_code", methods="DELETE")
     */
    public function delete($code, ValidatorInterface $validator, EntityManagerInterface $entityManager): Response
    {
        // funkcja usuwa z bazy kod tylko gdy format jest poprawny
        $numericalCode = new NumericalCode();
        $numericalCode->setCode($code);
        if (count($validator->validate($numericalCode)) > 0) {
            return $this->json('niepoprawny format kodu');
        }
        else{
            $repository = $entityManager->getRepository(NumericalCode::class);
            $numericalCode = $repository->findOneBy(["code" => $code]);

            if($numericalCode){
                $entityManager->remove($numericalCode);
                $entityManager->flush();
                return $this->json('usunięto kod ' . $code);
            }
            else{
                return $this->json('taki kod nie istnieje');
            }
        }
    }

    /**
     * @Route("/code/{code}", name="create_code", methods="POST")
     */
    public function create($code, ValidatorInterface $validator, EntityManagerInterface $entityManager): Response
    {
        // funkcja zapisuje przesłany kod tylko gdy format jest poprawny
        $numericalCode = new NumericalCode();
        $numericalCode->setCode($code);
        if (count($validator->validate($numericalCode)) > 0) {
            return $this->json('niepoprawny format kodu');
        }
        else{
            $numericalCode->setIsCorrect(true);
            $numericalCode->setCreatedAt(new \DateTimeImmutable());
            $entityManager->persist($numericalCode);
            $entityManager->flush();

            return $this->json('utworzono nowy kod');
        }
    }
}